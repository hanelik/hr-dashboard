import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Project } from "../model/project";

const PROJECTS: Project[] = [
  {
    id: 1,
    name: "nsdlkf",
    size: 34,
    clientName: "Client"
  },
  {
    id: 1,
    name: "nsdlkf",
    size: 34,
    clientName: "Client"
  }
];

@Injectable({
  providedIn: "root"
})
export class ProjectsService {
  $projects: BehaviorSubject<Project[]> = new BehaviorSubject([]);
  constructor() {}

  loadProjects() {
    this.$projects.next(PROJECTS);
  }
}
