import { Component, OnInit } from "@angular/core";
import { ProjectsService } from "./projects.service";
import { Project } from "../model/project";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.scss"]
})
export class ProjectsComponent implements OnInit {
  projects: Project[];
  displayedColumns: string[] = ["id", "name", "size", "clientName"];

  constructor(private projectService: ProjectsService) {
    this.projectService.loadProjects();
    this.projectService.$projects.subscribe(
      projects => (this.projects = projects)
    );
  }

  ngOnInit() {}
}
