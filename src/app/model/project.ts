export interface Project {
  id: number;
  name: string;
  size: number;
  clientName: string;
}
