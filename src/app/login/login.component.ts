import { Component, OnInit } from "@angular/core";
import { LoginService } from "./login.service";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginGroup: FormGroup;
  loginError: boolean;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.loginGroup = this.fb.group({
      username: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {}

  submitLogin() {
    this.loginService.login(this.login.value, this.password.value);
    this.loginError = true;
    if (this.loginService.isLoggedIn) {
      this.router.navigate(["dashboard"]);
    }
  }

  get login(): AbstractControl {
    return this.loginGroup.get("username");
  }

  get password(): AbstractControl {
    return this.loginGroup.get("password");
  }
}
