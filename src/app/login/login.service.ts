import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  private loggedIn: boolean;

  constructor() {}

  login(username: string, password: string) {
    if (username === "admin@admin.at" && password === "admin") {
      this.loggedIn = true;
    }
  }

  get isLoggedIn() {
    return this.loggedIn;
  }
}
